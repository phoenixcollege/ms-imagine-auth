Microsoft Imagine Authentication
==========

Handles MS Imagine WebStore external authentication.

Installation
-----------
* copy `bootstrap/config.php.copy` to `bootstrap/config.php`
    - modify the configuration strings to match your environment
* copy `layout/{file}.php.copy` to `layout/{file}.php`
    - modify the file contents to match your environment
* update the prefixes.txt with the class prefixes,names that students/faculty should have access
    - format is PREFIX,Full Name<LF>
    - based on ldap groups "PCC.2014.Fall.BIO.201.12345.Faculty/Student"

During authorization, the Auth class checks for school, matching year, a rough match for term and a matching prefix.
