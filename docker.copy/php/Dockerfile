FROM phoenixcollege/laravel-php-fpm:7.4

ARG WWW_USER=www-data
ARG WWW_GROUP=www-data
ARG USER_ID=1000
ARG GROUP_ID=1000
ARG XDEBUG_INSTALL=1

RUN \
  apk add --no-cache --virtual .persist-deps wget curl mariadb-client shadow bash

# PHP module configs
COPY ./conf/mods-available/ "$PHP_INI_DIR/conf.d/"

RUN \
  if [ "$XDEBUG_INSTALL" = 1 ]; then \
  echo "Installing Xdebug" && \
  apk add --no-cache --virtual .build-deps $PHPIZE_DEPS && \
  pecl install xdebug && \
  docker-php-ext-enable xdebug && \
  apk del -f .build-deps; \
  fi

COPY ./conf/zz-overrides-dev.ini "$PHP_INI_DIR/conf.d/zz-overrides-dev.ini"

RUN \
   groupmod -g ${GROUP_ID} ${WWW_GROUP} && \
   usermod -u ${USER_ID} ${WWW_USER} && \
   chown -R ${USER_ID}:${GROUP_ID} /app && \
   chmod -R 0775 /app

# XDebug port
EXPOSE 9000

VOLUME /app
WORKDIR /app
