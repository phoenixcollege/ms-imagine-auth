<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 1:45 PM
 */

namespace DreamSpark;

class Prefix
{

    protected $prefixes = [];

    public function __construct($prefix_string)
    {
        $this->setFromString($prefix_string);
    }

    public function getPrefixes()
    {
        return array_keys($this->prefixes);
    }

    public function setFromString($string, $lines = PHP_EOL, $parts = ',')
    {
        $line_array = explode($lines, $string);
        foreach ($line_array as $line) {
            $parts_array = array_map('trim', explode($parts, $line));
            if (isset($parts_array[0]) && $parts_array[0]) {
                $this->prefixes[$parts_array[0]] = $parts_array[1];
            }
        }
    }

    public function toArray()
    {
        return $this->prefixes;
    }
}
