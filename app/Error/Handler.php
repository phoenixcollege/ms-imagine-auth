<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 6:54 AM
 */

namespace DreamSpark\Error;

use DreamSpark\Debug;
use Psr\Log\LoggerInterface;
use Smorken\Redactor\Contracts\Redactor;

class Handler
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Smorken\Redactor\Contracts\Redactor
     */
    protected $redactor;

    public function __construct(LoggerInterface $logger, Redactor $redactor = null)
    {
        $this->logger = $logger;
        $this->redactor = $redactor;
        set_error_handler([$this, 'handleError']);
        set_exception_handler([$this, 'handleException']);
        register_shutdown_function([$this, 'handleShutdown']);
    }

    /**
     * Handle a PHP error for the application.
     *
     * @param  int    $level
     * @param  string $message
     * @param  string $file
     * @param  int    $line
     * @param  array  $context
     *
     * @throws \ErrorException
     */
    public function handleError($level, $message, $file = '', $line = 0, $context = [])
    {
        $error = error_get_last();
        if (!is_null($error)) {
            extract($error);
            if ($this->isFatal($type)) {
                if ($level) {
                    throw new \ErrorException($message, 0, $level, $file, $line);
                }
            }
        }
    }

    public function handleException($e)
    {
        try {
            $e = $this->redact($e);
            $n = [];
            $n['session'] = $this->redact($_SESSION);
            $n['request'] = $this->redact($_REQUEST);
            $n['server'] = $this->redact($_SERVER);
            $this->logger->error($e, $n);
        } catch (\Exception $e2) {
            //continue if not able to log
        }
        $this->displayError($e);
    }

    /**
     * Handle the PHP shutdown event.
     *
     * @return void
     */
    public function handleShutdown()
    {
        $error = error_get_last();

        // If an error has occurred that has not been displayed, we will create a fatal
        // error exception instance and pass it into the regular exception handling
        // code so it can be displayed back out to the developer for information.
        if (!is_null($error)) {
            extract($error);

            if (!$this->isFatal($type)) {
                return;
            }

            $this->handleException(new \ErrorException($type . '::' . $message, 0, $type, $file, $line));
        }
    }

    protected function displayError($e)
    {
        $errors = [];
        if (Debug::isDebug()) {
            $errors[] = a2str(
                [
                    'exception'   => get_class($e),
                    'code'        => $e->getCode(),
                    'message'     => $e->getMessage(),
                    'file'        => $e->getFile(),
                    'line_number' => $e->getLine(),
                    'trace'       => $e->getTraceAsString(),
                ]
            );
        } else {
            $errors[] = 'The server has experienced an error.  We will look into it as soon as possible.';
        }
        echo sprintf('<pre>%s</pre>', implode(PHP_EOL, $errors));
    }

    /**
     * Determine if the error type is fatal.
     *
     * @param  int $type
     * @return bool
     */
    protected function isFatal($type)
    {
        return in_array($type, [E_ERROR, E_CORE_ERROR, E_COMPILE_ERROR, E_PARSE]);
    }

    protected function redact($item)
    {
        if ($this->redactor) {
            return $this->redactor->findAndRedact($item);
        }
        return $item;
    }
}
