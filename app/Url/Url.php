<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 7:22 AM
 */

namespace DreamSpark\Url;

use DreamSpark\Auth\User;

class Url implements InterfaceUrl
{

    protected $config = [
        'host'       => 'https://e5.onthehub.com/WebStore/Security/AuthenticateUser.ashx',
        'key'        => null,
        'account'    => null,
        'member_org' => null,
        'shopper_ip' => null,
    ];

    public function __construct(array $config)
    {
        $this->config = array_replace($this->config, $config);
    }

    public function create(User $user)
    {
        $query = [
            'account'           => $this->config['account'],
            'username'          => null,
            'key'               => $this->config['key'],
            'academic_statuses' => null,
            'email'             => null,
            'last_name'         => null,
            'first_name'        => null,
            'shopper_ip'        => $this->config['shopper_ip'] ?: null,
        ];
        foreach ($user->toArray() as $k => $v) {
            $query[$k] = $v;
        }
        if ($this->config['member_org'] && $this->config['member_org']) {
            $query['member_org'] = $this->config['member_org'];
        }
        $query_string = $this->verifyAndBuildQuery($query);
        return sprintf('%s?%s', $this->config['host'], $query_string);
    }

    protected function verifyAndBuildQuery($params)
    {
        $required = [
            'account',
            'username',
            'key',
            'academic_statuses',
        ];
        foreach ($required as $k) {
            if (!isset($params[$k]) || !$params[$k]) {
                throw new UrlException("$k is a required parameter");
            }
        }
        return http_build_query($params);
    }
}
