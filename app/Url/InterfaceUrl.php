<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 7:20 AM
 */

namespace DreamSpark\Url;

use DreamSpark\Auth\User;

interface InterfaceUrl
{

    public function create(User $user);
}
