<?php

/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed  under the  License is distributed on an "AS IS" BASIS,
 * WITHOUT  WARRANTIES OR CONDITIONS  OF ANY KIND, either  express  or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2013 Scott Morken <scott.morken@phoenixcollege.edu>
 */

/**
 * Run Oct 23, 2013
 * Project: dreamspark
 *
 * @author Scott Morken <scott.morken@phoenixcollege.edu>
 */

namespace DreamSpark;

use DreamSpark\Auth\InterfaceAuth;
use DreamSpark\Http\InterfaceHttp;
use DreamSpark\Url\InterfaceUrl;

class Run
{

    /**
     * @var \DreamSpark\Auth\InterfaceAuth
     */
    private $auth;

    /**
     * @var \DreamSpark\Http\InterfaceHttp
     */
    private $http;

    /**
     * @var \DreamSpark\Url\InterfaceUrl
     */
    private $url;

    private $user_errors = [];

    public function __construct(InterfaceAuth $auth, InterfaceHttp $http, InterfaceUrl $url)
    {
        $this->auth = $auth;
        $this->http = $http;
        $this->url = $url;
    }

    public function getErrors()
    {
        return $this->user_errors;
    }

    public function hasErrors()
    {
        return count($this->user_errors) > 0;
    }

    public function run($username, $password)
    {
        $user = false;
        if (!is_null($username) && !is_null($password)) {
            $user = $this->auth->authenticate($username, $password);
        }
        if (Debug::isDebug()) {
            $this->user_errors['user'] = $user ? a2str($user->toArray()) : 'none';
        }
        if ($user) {
            $this->handleRedirect($user);
        } else {
            if ($this->auth->hasErrors()) {
                $this->user_errors += $this->auth->getErrors();
            }
        }
    }

    private function handleRedirect($user)
    {
        $url = $this->url->create($user);
        $handshake = new Handshake($this->http);
        $errors = $handshake->handshake($url);
        if ($errors) {
            $this->user_errors += $errors;
        }
    }

}
