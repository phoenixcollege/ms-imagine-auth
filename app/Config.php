<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 10:02 AM
 */

namespace DreamSpark;

class Config
{

    protected $config = [];

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function get($key, $default = null)
    {
        if (isset($this->config[$key])) {
            return $this->config[$key];
        }
        return $default;
    }

    public function set($key, $value)
    {
        $this->config[$key] = $value;
    }
}
