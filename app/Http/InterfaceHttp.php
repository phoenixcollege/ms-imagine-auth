<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/11/18
 * Time: 12:39 PM
 */

namespace DreamSpark\Http;

interface InterfaceHttp
{

    /**
     * @param $url
     * @return mixed
     */
    public function get($url);

    /**
     * @return array
     */
    public function getErrors();

    /**
     * @return bool
     */
    public function hasErrors();
}
