<?php
/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed  under the  License is distributed on an "AS IS" BASIS,
 * WITHOUT  WARRANTIES OR CONDITIONS  OF ANY KIND, either  express  or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2013 Scott Morken <scott.morken@phoenixcollege.edu>
 */

/**
 * Curl Oct 23, 2013
 * Project: dreamspark
 *
 * @author Scott Morken <scott.morken@phoenixcollege.edu>
 */

namespace DreamSpark\Http;

use DreamSpark\Debug;
use Psr\Log\LoggerInterface;

class Curl implements InterfaceHttp
{
    protected $errors = [];

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $log;

    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
    }

    public function get($url)
    {
        $ch = curl_init();
        curl_setopt_array(
            $ch,
            [
                CURLOPT_URL            => $url,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_FOLLOWLOCATION => 1,
            ]
        );
        $result = curl_exec($ch);
        if (curl_error($ch) != '') {
            $this->addError("Unable to connect to Microsoft Imagine.", true);
            $this->addError(curl_errno($ch) . ': ' . curl_error($ch));
        }
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($status >= 400) {
            $this->addError("Unable to connect to Microsoft Imagine.", true);
            $this->addError($status . ": " . $result);
        }
        curl_close($ch);
        if ($this->hasErrors()) {
            return false;
        }
        return $result;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function hasErrors()
    {
        return count($this->errors) > 0;
    }

    protected function addError($message, $usermessage = false)
    {
        if ($usermessage == true) {
            $this->errors[] = $message;
        } else {
            if (Debug::isDebug()) {
                $this->errors[] = $message;
            }
            $this->log->error($message);
        }
    }
}
