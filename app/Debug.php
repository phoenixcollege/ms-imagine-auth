<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 7:01 AM
 */

namespace DreamSpark;

class Debug
{

    protected static $debug = false;

    public static function disable()
    {
        self::$debug = false;
    }

    public static function enable()
    {
        self::$debug = true;
    }

    /**
     * @return bool
     */
    public static function isDebug()
    {
        return self::$debug;
    }

    public static function set($debug)
    {
        self::$debug = (bool)$debug;
    }
}
