<?php

/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed  under the  License is distributed on an "AS IS" BASIS,
 * WITHOUT  WARRANTIES OR CONDITIONS  OF ANY KIND, either  express  or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2013 Scott Morken <scott.morken@phoenixcollege.edu>
 */

/**
 * AbstractAuth Oct 23, 2013
 * Project: dreamspark
 *
 * @author Scott Morken <scott.morken@phoenixcollege.edu>
 */

namespace DreamSpark\Auth;

use Psr\Log\LoggerInterface;

abstract class AbstractAuth implements \DreamSpark\Auth\InterfaceAuth
{

    protected $avail_statuses = [
        'staff',
        'students',
        'faculty',
    ];

    protected $allowed = [];

    protected $errors = [];

    protected $options = [];

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $log;

    public function __construct(LoggerInterface $log, $options, $allowed = [])
    {
        $this->log = $log;
        $this->allowed = $allowed;
        $this->initOptions($options);
    }

    public function authenticate($username, $password)
    {
        if ($username && $password) {
            $user = $this->_authenticate($username, $password);
            if ($user instanceof \DreamSpark\Auth\User) {
                if ($this->authorizeUser($user)) {
                    return $user;
                }
            }
        } else {
            $this->addError("Incorrect username or password", true);
        }
        return false;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function hasErrors()
    {
        return count($this->errors) > 0;
    }

    protected abstract function _authenticate($username, $password);

    protected function addError($message, $usermessage = false)
    {
        if ($usermessage == true) {
            $this->errors[] = $message;
        } else {
            $this->log->error($message);
        }
    }

    protected abstract function authorizeUser(User &$user);

    protected function getOption($key, $default = null)
    {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }
        return $default;
    }

    protected function initOptions($options)
    {
        $this->options = $options;
    }
}
