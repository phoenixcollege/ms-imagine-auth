<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/11/18
 * Time: 12:06 PM
 */

namespace DreamSpark\Auth\Proxy;

use DreamSpark\Auth\AbstractAuth;
use DreamSpark\Auth\InterfaceAuth;
use DreamSpark\Auth\User;
use Psr\Log\LoggerInterface;
use Smorken\Auth\Proxy\Common\Contracts\Exception;
use Smorken\Auth\Proxy\Common\Contracts\Models\Response;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Common\Exceptions\SystemException;

class Auth extends AbstractAuth implements InterfaceAuth
{

    /**
     * @var \Smorken\Auth\Proxy\Common\Contracts\Provider
     */
    protected $provider;

    protected $map = [
        'username'   => 'username',
        'first_name' => 'first_name',
        'last_name'  => 'last_name',
        'email'      => 'email',
    ];

    protected $staff_groups;

    public function __construct(Provider $provider, LoggerInterface $log, $options, array $allowed = [])
    {
        $this->provider = $provider;
        parent::__construct($log, $options, $allowed);
    }

    protected function _authenticate($username, $password)
    {
        try {
            $response = $this->provider->authenticate($username, $password);
            if ($response->isAuthenticated()) {
                return $this->parseUser($response);
            } else {
                $this->addError($response->message ?: 'Unable to authenticate. Check your username/password.', true);
            }
        } catch (\Exception $e) {
            if ($e instanceof Exception) {
                $this->addError($e->display(), true);
                if ($e instanceof SystemException) {
                    $this->addError($e->getMessage());
                }
            } else {
                $this->addError($e->getMessage());
            }
        }
        return false;
    }

    protected function authorizeUser(User &$user)
    {
        $authorized = false;
        if ($user) {
            foreach ($user->groups as $group) {
                $result = $this->validateGroup($group);
                if ($result) {
                    $authorized = true;
                    if (!in_array($result, $user->statuses)) {
                        $user->statuses[] = $result;
                    }
                }
            }
        }
        if (!$authorized) {
            $this->addError("You do not appear to be currently enrolled in STEM courses at PC.", true);
            $user = false;
        }
        return $authorized;
    }

    protected function checkClass($cls, $year, $month)
    {
        $alloweddates = [
            'fal' => [8, 9, 10, 11, 12],
            'spr' => [1, 2, 3, 4, 5],
            'sum' => [5, 6, 7, 8],
        ];
        if (in_array($cls[0], $this->getOption('schools', []))) {
            $termkey = strtolower(substr($cls[2], 0, 3));
            if ($cls[1] == $year && in_array($month, $alloweddates[$termkey])) {
                foreach ($this->allowed as $prefix) {
                    $p = trim(strtoupper($prefix));
                    if ($cls[3] == $p) {
                        return $this->getStatusType(end($cls));
                    }
                }
            }
        }
        return false;
    }

    protected function getStaffGroups()
    {
        if (!$this->staff_groups) {
            $this->staff_groups = $this->getOption('staff_groups', []);
        }
        return $this->staff_groups;
    }

    protected function getStatusType($str)
    {
        $type = "";
        switch (strtolower($str)) {
            case 'faculty':
                $type = 'faculty';
                break;
            case 'student':
                $type = 'students';
                break;
        }
        return $type;
    }

    protected function parseUser(Response $response)
    {
        $ruser = $response->user;
        $duser = new \DreamSpark\Auth\User();
        foreach ($this->map as $ruserattr => $duserattr) {
            $duser->$duserattr = $ruser->$ruserattr;
        }
        if ($ruser->data && isset($ruser->data['groups'])) {
            $duser->groups = $ruser->data['groups'];
        }
        return $duser;
    }

    protected function validateGroup($group)
    {
        $pattern = '/^[A-Z]{3}\.[0-9]{4}\.[A-z]+\.[A-Z]{3,8}\.[\w-]{3,9}\.[0-9]{5}\.[A-z]+$/';
        $year = date('Y');
        $month = date('n');
        if (preg_match($pattern, $group)) {
            $splitclass = explode('.', $group);
            return $this->checkClass($splitclass, $year, $month);
        }
        if (in_array($group, $this->getStaffGroups())) {
            return 'staff';
        }
        return false;
    }
}
