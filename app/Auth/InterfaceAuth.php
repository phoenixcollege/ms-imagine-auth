<?php
/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed  under the  License is distributed on an "AS IS" BASIS,
 * WITHOUT  WARRANTIES OR CONDITIONS  OF ANY KIND, either  express  or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2013 Scott Morken <scott.morken@phoenixcollege.edu>
 */

/**
 * InterfaceAuth Oct 23, 2013
 * Project: dreamspark
 *
 * @author Scott Morken <scott.morken@phoenixcollege.edu>
 */

namespace DreamSpark\Auth;

/**
 * Interface InterfaceAuth
 *
 * @package DreamSpark\Auth
 *
 * @property bool $isLoggedIn
 */
interface InterfaceAuth
{

    public function authenticate($username, $password);

    /**
     * @return array
     */
    public function getErrors();

    /**
     * @return bool
     */
    public function hasErrors();
}
