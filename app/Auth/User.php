<?php
/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed  under the  License is distributed on an "AS IS" BASIS,
 * WITHOUT  WARRANTIES OR CONDITIONS  OF ANY KIND, either  express  or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2013 Scott Morken <scott.morken@phoenixcollege.edu>
 */

/**
 * User Oct 23, 2013
 * Project: dreamspark
 *
 * @author Scott Morken <scott.morken@phoenixcollege.edu>
 */

namespace DreamSpark\Auth;

class User
{
    public $first_name;
    public $last_name;
    public $email;
    public $username;
    public $groups = [];
    public $statuses = [];

    public function isValid()
    {
        return $this->first_name && $this->last_name && $this->email && $this->username && $this->statuses;
    }

    public function statuses($as_string = true)
    {
        if ($as_string) {
            return implode(',', $this->statuses);
        }
        return $this->statuses;
    }

    public function toArray()
    {
        return [
            'first_name'        => $this->first_name,
            'last_name'         => $this->last_name,
            'email'             => $this->email,
            'username'          => $this->username,
            'academic_statuses' => $this->statuses(),
        ];
    }
}
