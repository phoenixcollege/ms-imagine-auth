<?php

/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed  under the  License is distributed on an "AS IS" BASIS,
 * WITHOUT  WARRANTIES OR CONDITIONS  OF ANY KIND, either  express  or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2013 Scott Morken <scott.morken@phoenixcollege.edu>
 */

/**
 * Auth Oct 23, 2013
 * Project: dreamspark
 *
 * @author Scott Morken <scott.morken@phoenixcollege.edu>
 */

namespace DreamSpark\Auth\Cims;

class Auth extends \DreamSpark\Auth\AbstractAuth
{

    protected $server;
    protected $port = 389;
    protected $bindUser = null;
    protected $bindPw = null;
    protected $baseDn;
    protected $attributes = [
        'dn',
        'sn',
        'givenName',
        'samAccountName',
        'mail',
        'memberOf',
    ];
    protected $filter = 'samaccountname=%s';
    protected $conn;
    protected $bind;

    public function __destruct()
    {
        $this->close();
    }

    public function authorizeUser(\DreamSpark\Auth\User &$user)
    {
        $authorized = false;
        if ($user) {
            foreach ($user->groups as $group) {
                $result = $this->validateGroup($group);
                if ($result) {
                    $authorized = true;
                    if (!in_array($result, $user->statuses)) {
                        $user->statuses[] = $result;
                    }
                }
            }
        }
        if (!$authorized) {
            $this->addError("You do not appear to be currently enrolled in STEM courses at PC.", true);
            $user = false;
        }
        return $authorized;
    }

    protected function _authenticate($username, $password)
    {
        $this->connect();
        if ($username && $password && $this->getConnection()) {
            if ($this->bindUser && $this->bindPw) {
                if (!$this->bind($this->bindUser, $this->bindPw)) {
                    $this->addError("Admin Bind: " . ldap_error($this->conn));
                    $this->addError("Could not contact the authentication server", true);
                }
            }
            if (!$this->hasErrors()) {
                $searchString = ldap_escape(sprintf($this->filter, $username));
                $r = ldap_search($this->conn, $this->baseDn, $searchString, $this->attributes);
                return $this->handleResults($r, $password);
            }
        }
        return false;
    }

    protected function bind($dn, $password)
    {
        if ($this->getConnection()) {
            $this->bind = ldap_bind($this->conn, $dn, $password);
            if (DEBUG) {
                echo "Bind Result: $dn -> " . $this->bind . "\n";
            }
            if (!$this->bind) {
                if (ldap_errno($this->conn) != 49) { //invalid credentials
                    $this->addError(ldap_errno($this->conn) . ': ' . ldap_error($this->conn));
                }
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    protected function checkClass($cls, $year, $month)
    {
        $alloweddates = [
            'fal' => [8, 9, 10, 11, 12],
            'spr' => [1, 2, 3, 4, 5],
            'sum' => [5, 6, 7, 8],
        ];
        if (in_array($cls[0], $this->getOption('schools', []))) {
            $termkey = strtolower(substr($cls[2], 0, 3));
            if ($cls[1] == $year && in_array($month, $alloweddates[$termkey])) {
                foreach ($this->allowed as $prefix) {
                    $p = trim(strtoupper($prefix));
                    if ($cls[3] == $p) {
                        return $this->getStatusType(end($cls));
                    }
                }
            }
        }
        return false;
    }

    protected function close()
    {
        if ($this->conn) {
            ldap_close($this->conn);
        }
    }

    protected function connect()
    {
        $this->conn = ldap_connect($this->server, $this->port);
        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
    }

    protected function getBind()
    {
        if ($this->bind) {
            return $this->bind;
        } else {
            return false;
        }
    }

    protected function getConnection()
    {
        if ($this->conn) {
            return $this->conn;
        } else {
            return false;
        }
    }

    protected function getStatusType($cimsString)
    {
        $type = "";
        switch (strtolower($cimsString)) {
            case 'faculty':
                $type = 'faculty';
                break;
            case 'student':
                $type = 'students';
                break;
        }
        return $type;
    }

    protected function handleResults($searchhandle, $password)
    {
        if ($searchhandle) {
            $results = ldap_get_entries($this->conn, $searchhandle);
            if (is_array($results) && isset($results[0])) {
                $dn = $results[0]['dn'];
                if ($this->bind($dn, $password)) {
                    return $this->parseUser($results[0]);
                }
            }
        }
        $this->addError('Incorrect MEID and/or password.', true);
        return false;
    }

    protected function initOptions($options)
    {
        $required = ['server', 'baseDn'];
        foreach ($options as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
        foreach ($required as $key) {
            if (!$this->$key) {
                throw new \Exception("Property $key is a required option.");
            }
        }
    }

    protected function parseUser($results)
    {
        $user = new \DreamSpark\Auth\User();
        $user->email = $results['mail'][0];
        $user->first_name = $results['givenname'][0];
        $user->last_name = $results['sn'][0];
        $user->username = $results['samaccountname'][0];
        $user->groups = $results['memberof'];
        return $user;
    }

    protected function validateGroup($group)
    {
        $pattern = '/^[A-Z]{3}\.[0-9]{4}\.[A-z]+\.[A-Z]{3,8}\.[0-9A-Z]{3,5}\.[0-9]{5}\.[A-z]+$/';
        $year = date('Y');
        $month = date('n');

        if (substr($group, 0, 3) == 'CN=') {
            $group = substr($group, 3);
            $gexp = explode(',', $group);
            $cn = $gexp[0];
            if (preg_match($pattern, $cn)) {
                $splitclass = explode('.', $cn);
                return $this->checkClass($splitclass, $year, $month);
            }
            if (in_array($cn, $this->getOption('staff_groups', []))) {
                return 'staff';
            }
        }
        return false;
    }

}
