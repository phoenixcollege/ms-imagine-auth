<?php

/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed  under the  License is distributed on an "AS IS" BASIS,
 * WITHOUT  WARRANTIES OR CONDITIONS  OF ANY KIND, either  express  or
 * implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2013 Scott Morken <scott.morken@phoenixcollege.edu>
 */

/**
 * Handshake Oct 23, 2013
 * Project: dreamspark
 *
 * @author Scott Morken <scott.morken@phoenixcollege.edu>
 */

namespace DreamSpark;

use DreamSpark\Http\InterfaceHttp;

class Handshake
{

    /**
     * @var \DreamSpark\Http\InterfaceHttp
     */
    protected $http;

    public function __construct(InterfaceHttp $http)
    {
        $this->http = $http;
    }

    public function handshake($url)
    {
        $redirect = $this->http->get($url);
        if ($redirect !== false) {
            if (Debug::isDebug()) {
                return ['url' => $url, 'redirect' => $redirect];
            } else {
                session_write_close();
                header('Location: ' . $redirect);
                exit(0);
            }
        } else {
            $ret = $this->http->getErrors();
            if (Debug::isDebug()) {
                $ret['url'] = $url;
            }
            return $ret;
        }
    }
}
