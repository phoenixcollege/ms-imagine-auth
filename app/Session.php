<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 11:35 AM
 */

namespace DreamSpark;

class Session
{

    protected $base = 'ds_sess';

    protected $unset = [];

    public function __construct()
    {
        $this->start();
    }

    public function __destruct()
    {
        $this->stop();
    }

    public function clear()
    {
        if ($this->isStarted()) {
            $_SESSION = [];
        }
    }

    public function get($key, $default = null)
    {
        $this->unset[$key] = true;
        if ($this->has($key)) {
            $key = $this->getKey($key);
            return $_SESSION[$key];
        }
        return $default;
    }

    public function has($key)
    {
        $key = $this->getKey($key);
        return isset($_SESSION[$key]);
    }

    public function isStarted()
    {
        return session_id() !== '';
    }

    public function set($key, $value)
    {
        $key = $this->getKey($key);
        $_SESSION[$key] = $value;
    }

    public function start()
    {
        if (!$this->isStarted()) {
            $this->setDefaultParams();
            session_start();
        }
    }

    public function stop()
    {
        if ($this->isStarted()) {
            foreach ($this->unset as $k => $go) {
                if ($k && $go) {
                    $this->unset($k);
                }
            }
            session_write_close();
        }
    }

    public function unset($key)
    {
        $key = $this->getKey($key);
        unset($_SESSION[$key]);
    }

    protected function getKey($key)
    {
        return sprintf('%s::%s', $this->base, $key);
    }

    protected function setDefaultParams()
    {
        $current = session_get_cookie_params();
        $current['httponly'] = true;
        $current['secure'] = true;
        if (PHP_VERSION_ID < 70300) {
            session_set_cookie_params($current['lifetime'], $current['path'].'; samesite=None', $current['domain'],
                $current['secure'], $current['httponly']);
        } else {
            $current['samesite'] = 'None';
            session_set_cookie_params($current);
        }
    }
}
