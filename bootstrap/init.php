<?php
require __DIR__.'/_autoloader.php';

if (!ini_get('date.timezone')) {
    ini_set('date.timezone', 'America/Phoenix');
}

$config = new \DreamSpark\Config(require __DIR__.'/config.php');

\DreamSpark\Debug::set($config->get('debug', false));

if (\DreamSpark\Debug::isDebug()) {
    error_reporting(-1);
} else {
    error_reporting(E_ERROR | E_COMPILE_ERROR | E_CORE_ERROR | E_USER_ERROR);
}

$log = new \Monolog\Logger('msimagine');
$log->pushHandler(new \Monolog\Handler\StreamHandler(__DIR__.'/../logs/app.log',
        \DreamSpark\Debug::isDebug() ? \Monolog\Logger::DEBUG : \Monolog\Logger::WARNING));
if (!\DreamSpark\Debug::isDebug()) {
    $log->pushHandler(new \Monolog\Handler\NativeMailerHandler($config->get('email'), 'MS Imagine Exception',
            $config->get('from'), \Monolog\Logger::ERROR));
}

$rc = $config->get('redactor', []);
$redactor = \Smorken\Redactor\Factory::fromConfig($rc);

$errorhandler = new \DreamSpark\Error\Handler($log, $redactor);

$session = new \DreamSpark\Session();

$prefix = new \DreamSpark\Prefix($config->get('prefix_string', ''));
