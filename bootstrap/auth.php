<?php
//$auth = new \DreamSpark\Auth\Cims\Auth($options['ldap'], $allowed);

$provider = new \Smorken\Auth\Proxy\Common\Providers\Guzzle(new \GuzzleHttp\Client(), $config->get('proxy', []));
$auth = new \DreamSpark\Auth\Proxy\Auth($provider, $log, $config->get('auth', []), $prefix->getPrefixes());

$http = new \DreamSpark\Http\Curl($log);

$url = new DreamSpark\Url\Url($config->get('url', []));

if (isset($_GET['action']) && $_GET['action'] === 'signout') {
    $session->clear();
    $messages[] = 'You have been logged out.';
} elseif (isset($_POST) && count($_POST)) {
    $run = new \DreamSpark\Run($auth, $http, $url);
    $run->run(
        isset($_POST['username']) ? $_POST['username'] : null,
        isset($_POST['password']) ? $_POST['password'] : null
    );
    $session->set('errors', $run->getErrors());
    unset($_POST);
    $session->stop();
    header('Location: index.php');
    exit(0);
}
