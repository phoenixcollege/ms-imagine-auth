<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 8:07 AM
 */

namespace Tests\DreamSpark\unit\Url;

use DreamSpark\Url\Url;
use DreamSpark\Url\UrlException;
use PHPUnit\Framework\TestCase;

class UrlTest extends TestCase
{

    use \Tests\DreamSpark\unit\User;

    public function testCreateOk()
    {
        $sut = $this->getSut();
        $url = $sut->create($this->getUser());
        $expected = 'https://host.com?account=abc&username=foouser&key=1234&academic_statuses=status1&email=email%40example.org&last_name=bar&first_name=foo&shopper_ip=1.1.1.1&member_org=foo';
        $this->assertEquals($expected, $url);
    }

    public function testMissingRequiredIsException()
    {
        $user = $this->getUser();
        $user->username = null;
        $sut = $this->getSut();
        $this->expectException(UrlException::class);
        $url = $sut->create($user);
    }

    public function testMissingOptionalIsOk()
    {
        $user = $this->getUser();
        $user->email = null;
        $user->first_name = null;
        $user->last_name = null;
        $sut = $this->getSut();
        $url = $sut->create($user);
        $expected = 'https://host.com?account=abc&username=foouser&key=1234&academic_statuses=status1&shopper_ip=1.1.1.1&member_org=foo';
        $this->assertEquals($expected, $url);
    }

    protected function getConfig()
    {
        return [
            'host'       => 'https://host.com',
            'key'        => '1234',
            'account'    => 'abc',
            'member_org' => 'foo',
            'shopper_ip' => '1.1.1.1',
        ];
    }

    protected function getSut($config = null)
    {
        if (is_null($config)) {
            $config = $this->getConfig();
        }
        return new Url($config);
    }
}
