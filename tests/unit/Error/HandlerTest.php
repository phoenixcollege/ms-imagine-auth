<?php

namespace Tests\DreamSpark\unit\Error;

use DreamSpark\Error\Handler;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Smorken\Redactor\Factory;
use Smorken\Redactor\Types\Exception;
use Smorken\Redactor\Types\RegEx;

class ExceptionStub
{
    public function authenticate($username, $password)
    {
        throw new \Exception('Authenticate exception');
    }

    public function connect($dsn, $username, $password)
    {
        throw new \Exception('Connect exception');
    }
}

class HandlerTest extends TestCase
{

    public function testRedactedException()
    {
        list($sut, $logger) = $this->getSut();
        $es = new ExceptionStub();
        try {
            $es->authenticate('foo', 'mypassword');
        } catch (\Exception $e) {
            $sut->handleException($e);
            $this->assertEquals('[ REDACTED ]', $e->getTrace()[0]['args'][1]);
            $this->assertEquals('foo', $e->getTrace()[0]['args'][0]);
        }
    }

    protected function getRedactor(array $config = [])
    {
        if (empty($config)) {
            $config = [
                'active' => true,
                'types'  => [
                    RegEx::class     => [
                        [
                            [
                                '/.*pass.*/i',
                                '/"pass(.*?)"\s*\:\s*"(.*?)"/i',
                                '/.*key.*/i',
                                '/.*salt.*/i',
                                '/.*token.*/i',
                            ],
                        ],
                    ],
                    Exception::class => [
                        [
                            '*::authenticate',
                            [1],
                        ],
                        [
                            '*::connect',
                            [],
                        ],
                    ],
                ],
            ];
        }
        return Factory::fromConfig($config);
    }

    protected function getSut()
    {
        $logger = $this->getMockBuilder(LoggerInterface::class)
                       ->getMock();
        return [new Handler($logger, $this->getRedactor()), $logger];
    }
}
