<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 8:57 AM
 */

namespace Tests\DreamSpark\unit\Auth\Proxy;

use DreamSpark\Auth\Proxy\Auth;
use DreamSpark\Debug;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Common\Models\Response;
use Smorken\Auth\Proxy\Common\Models\User;

class AuthTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        Debug::enable();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testAuthenticateFalseWithErrorResponse()
    {
        list($sut, $provider, $log) = $this->getSut();
        $provider->shouldReceive('authenticate')
                 ->once()
                 ->with('foouser', 'barpass')
                 ->andReturn($this->getErrorAuthenticatedResponse());
        $user = $sut->authenticate('foouser', 'barpass');
        $this->assertFalse($user);
    }

    public function testAuthenticateFalseWithNonAuthenticatedResponse()
    {
        $groups = [
            sprintf('PCC.%d.%s.ABC.100.12345.Student', date('Y'), $this->getCurrentTermName()),
        ];
        list($sut, $provider, $log) = $this->getSut();
        $provider->shouldReceive('authenticate')
                 ->once()
                 ->with('foouser', 'barpass')
                 ->andReturn($this->getNonAuthenticatedResponse($groups));
        $user = $sut->authenticate('foouser', 'barpass');
        $this->assertFalse($user);
    }

    public function testAuthenticateFalseWithoutValidGroup()
    {
        list($sut, $provider, $log) = $this->getSut();
        $provider->shouldReceive('authenticate')
                 ->once()
                 ->with('foouser', 'barpass')
                 ->andReturn($this->getAuthenticatedResponse());
        $user = $sut->authenticate('foouser', 'barpass');
        $this->assertFalse($user);
    }

    public function testAuthenticateOk()
    {
        $groups = [
            sprintf('PCC.%d.%s.ABC.100.12345.Student', date('Y'), $this->getCurrentTermName()),
        ];
        list($sut, $provider, $log) = $this->getSut();
        $provider->shouldReceive('authenticate')
                 ->once()
                 ->with('foouser', 'barpass')
                 ->andReturn($this->getAuthenticatedResponse($groups));
        $user = $sut->authenticate('foouser', 'barpass');
        $this->assertInstanceOf(\DreamSpark\Auth\User::class, $user);
        $this->assertTrue($user->isValid());
    }

    protected function getAuthenticatedResponse($groups = [])
    {
        $user = new User(
            [
                'id'         => 1,
                'username'   => 'foouser',
                'first_name' => 'foo',
                'last_name'  => 'bar',
                'email'      => 'email@example.org',
                'data'       => ['groups' => $groups],
            ]
        );
        return (new Response())->fromUser($user, true);
    }

    protected function getCurrentTermName()
    {
        $curr = date('n');
        if ($curr <= 5) {
            return 'Spring';
        }
        if ($curr <= 8) {
            return 'SummerI';
        }
        return 'Fall';
    }

    protected function getErrorAuthenticatedResponse()
    {
        return (new Response())->fromException(new \Exception('Auth Exception'));
    }

    protected function getNonAuthenticatedResponse($groups = [])
    {
        $user = new User(
            [
                'id'         => 1,
                'username'   => 'foouser',
                'first_name' => 'foo',
                'last_name'  => 'bar',
                'email'      => 'email@example.org',
                'data'       => ['groups' => $groups],
            ]
        );
        return (new Response())->fromUser($user, false);
    }

    protected function getSut()
    {
        $provider = m::mock(Provider::class);
        $log = m::mock(LoggerInterface::class);
        $allowed = ['ABC'];
        $config = [
            'staff_groups' => [
                'FOO',
            ],
            'schools'      => [
                'PCC',
            ],
        ];
        $sut = new Auth($provider, $log, $config, $allowed);
        return [$sut, $provider, $log];
    }
}
