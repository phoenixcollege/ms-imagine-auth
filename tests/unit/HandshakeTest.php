<?php

namespace Tests\DreamSpark\unit;

use DreamSpark\Debug;
use DreamSpark\Handshake;
use DreamSpark\Http\InterfaceHttp;
use PHPUnit\Framework\TestCase;
use Mockery as m;

class HandshakeTest extends TestCase
{

    public function setUp(): void
    {
        Debug::enable();
        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testHandshakeRedirectsOnOk()
    {
        list($sut, $http) = $this->getSut();
        $url = 'http://url';
        $http->shouldReceive('get')->once()->with($url)->andReturn('http://redirect');
        $redirect = $sut->handshake('http://url');
        $expected = ['url' => $url, 'redirect' => 'http://redirect'];
        $this->assertEquals($expected, $redirect);
    }

    public function testHandshakeReturnsErrorsOnHttpError()
    {
        list($sut, $http) = $this->getSut();
        $url = 'http://url';
        $http->shouldReceive('get')->once()->with($url)->andReturn(false);
        $http->shouldReceive('getErrors')->once()->andReturn(['foo error']);
        $this->assertEquals(['url' => $url, 'foo error'], $sut->handshake($url));
    }

    protected function getSut()
    {
        $http = m::mock(InterfaceHttp::class);
        $sut = new Handshake($http);
        return [$sut, $http];
    }
}
