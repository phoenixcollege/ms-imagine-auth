<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 8:44 AM
 */

namespace Tests\DreamSpark\unit;

trait User
{

    protected function getUser(
        $attrs = [
            'username'   => 'foouser',
            'first_name' => 'foo',
            'last_name'  => 'bar',
            'email'      => 'email@example.org',
            'statuses'   => ['status1'],
        ]
    ) {
        $user = new \DreamSpark\Auth\User();
        foreach ($attrs as $k => $v) {
            $user->$k = $v;
        }
        return $user;
    }
}
