<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 2:16 PM
 */

namespace Tests\DreamSpark\unit;

use DreamSpark\Prefix;
use PHPUnit\Framework\TestCase;

class PrefixTest extends TestCase
{

    public function testCreatesPrefixesFromString()
    {
        $str = "ABC,ABC Course\nDEF,DEF Course";
        $sut = new Prefix($str);
        $expected = [
            'ABC' => 'ABC Course',
            'DEF' => 'DEF Course',
        ];
        $this->assertEquals($expected, $sut->toArray());
    }

    public function testCreatesPrefixesFromStringAndTrims()
    {
        $str = " ABC ,\tABC Course    \n\tDEF,   DEF Course\n";
        $sut = new Prefix($str);
        $expected = [
            'ABC' => 'ABC Course',
            'DEF' => 'DEF Course',
        ];
        $this->assertEquals($expected, $sut->toArray());
    }

    public function testGetPrefixesReturnsArrayOfPrefixes()
    {
        $str = "ABC,ABC Course\nDEF,DEF Course";
        $sut = new Prefix($str);
        $expected = [
            'ABC',
            'DEF',
        ];
        $this->assertEquals($expected, $sut->getPrefixes());
    }
}
