<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 10/12/18
 * Time: 9:25 AM
 */

namespace Tests\DreamSpark\integration;

use DreamSpark\Auth\Proxy\Auth;
use DreamSpark\Debug;
use DreamSpark\Http\InterfaceHttp;
use DreamSpark\Run;
use DreamSpark\Url\Url;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Smorken\Auth\Proxy\Common\Contracts\Provider;
use Smorken\Auth\Proxy\Common\Models\Response;
use Smorken\Auth\Proxy\Common\Models\User;

class RunTest extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        Debug::enable();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    public function testRunAuthenticateError()
    {
        list($sut, $provider, $http, $log) = $this->getSut();
        $provider->shouldReceive('authenticate')
                 ->once()
                 ->with('foouser', 'barpass')
                 ->andReturn($this->getErrorAuthenticatedResponse());
        $http->shouldReceive('get')
             ->never();
        $sut->run('foouser', 'barpass');
        $errors = $sut->getErrors();
        $this->assertEquals($errors['user'], 'none');
        $this->assertEquals($errors[0], 'There was an error connecting to the authentication provider.');
    }

    public function testRunAuthenticateFailure()
    {
        list($sut, $provider, $http, $log) = $this->getSut();
        $provider->shouldReceive('authenticate')
                 ->once()
                 ->with('foouser', 'barpass')
                 ->andReturn($this->getNonAuthenticatedResponse());
        $http->shouldReceive('get')
             ->never();
        $sut->run('foouser', 'barpass');
        $errors = $sut->getErrors();
        $this->assertEquals($errors['user'], 'none');
        $this->assertEquals($errors[0], 'Unable to authenticate. Check your username/password.');
    }

    public function testRunAuthenticateSuccess()
    {
        list($sut, $provider, $http, $log) = $this->getSut();
        $groups = [
            $this->getValidGroup(),
        ];
        $provider->shouldReceive('authenticate')
                 ->once()
                 ->with('foouser', 'barpass')
                 ->andReturn($this->getAuthenticatedResponse($groups));
        $http->shouldReceive('get')
             ->once()
             ->with(
                 'https://host.com?account=abc&username=foouser&key=1234&academic_statuses=students&email=email%40example.org&last_name=bar&first_name=foo&shopper_ip=1.1.1.1&member_org=foo'
             )
             ->andReturn('http://redirect');
        $sut->run('foouser', 'barpass');
        $errors = $sut->getErrors();
        $this->assertStringStartsWith('first_name:', $errors['user']);
        $this->assertEquals('http://redirect', $errors['redirect']);
    }

    public function testRunAuthenticateSuccessNoGroup()
    {
        list($sut, $provider, $http, $log) = $this->getSut();
        $provider->shouldReceive('authenticate')
                 ->once()
                 ->with('foouser', 'barpass')
                 ->andReturn($this->getAuthenticatedResponse());
        $http->shouldReceive('get')
             ->never();
        $sut->run('foouser', 'barpass');
        $errors = $sut->getErrors();
        $this->assertEquals($errors['user'], 'none');
        $this->assertEquals($errors[0], 'You do not appear to be currently enrolled in STEM courses at PC.');
    }

    protected function getAuth($provider, $log)
    {
        $config = [
            'staff_groups' => [
                'FOO',
            ],
            'schools'      => [
                'PCC',
            ],
        ];
        return new Auth($provider, $log, $config, ['ABC']);
    }

    protected function getAuthenticatedResponse($groups = [])
    {
        $user = new User(
            [
                'id'         => 1,
                'username'   => 'foouser',
                'first_name' => 'foo',
                'last_name'  => 'bar',
                'email'      => 'email@example.org',
                'data'       => ['groups' => $groups],
            ]
        );
        return (new Response())->fromUser($user, true);
    }

    protected function getCurrentTermName()
    {
        $curr = date('n');
        if ($curr <= 5) {
            return 'Spring';
        }
        if ($curr <= 8) {
            return 'SummerI';
        }
        return 'Fall';
    }

    protected function getErrorAuthenticatedResponse()
    {
        return (new Response())->fromException(new \Exception('Auth Exception'));
    }

    protected function getNonAuthenticatedResponse($groups = [])
    {
        $user = new User(
            [
                'id'         => 1,
                'username'   => 'foouser',
                'first_name' => 'foo',
                'last_name'  => 'bar',
                'email'      => 'email@example.org',
                'data'       => ['groups' => $groups],
            ]
        );
        return (new Response())->fromUser($user, false);
    }

    protected function getSut()
    {
        $provider = m::mock(Provider::class);
        $http = m::mock(InterfaceHttp::class);
        $log = m::mock(LoggerInterface::class);
        $sut = new Run($this->getAuth($provider, $log), $http, $this->getUrl());
        return [$sut, $provider, $http, $log];
    }

    protected function getUrl()
    {
        $config = [
            'host'       => 'https://host.com',
            'key'        => '1234',
            'account'    => 'abc',
            'member_org' => 'foo',
            'shopper_ip' => '1.1.1.1',
        ];
        return new Url($config);
    }

    protected function getValidGroup($type = 'Student')
    {
        return sprintf('PCC.%d.%s.ABC.100.12345.%s', date('Y'), $this->getCurrentTermName(), $type);
    }
}
